This is a very simple NextJS Project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app). Modified to be a PokeDex.

![Live Demo](https://i.imgur.com/9Q2JwJb.png)

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Live Demo

[`NextJS PokeDex - Live Demo`](https://pokedex-weld.vercel.app/)
