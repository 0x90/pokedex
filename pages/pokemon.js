import React from "react";
import Layout from "../components/Layout";
import Link from "next/link";

const TypeToColor = (color) => {
  switch (color) {
    case "normal":
      color = "gray-600";
      break;
    case "grass":
      color = "green-600";
      break;
    case "fire":
      color = "red-600";
      break;
    case "water":
      color = "blue-600";
      break;
    case "fighting":
      color = "yellow-800";
      break;
    case "flying":
      color = "blue-400";
      break;
    case "poison":
      color = "pink-700";
      break;
    case "ground":
      color = "yellow-500";
      break;
    case "rock":
      color = "orange-700";
      break;
    case "bug":
      color = "yellow-400";
      break;
    case "ghost":
      color = "blue-800";
      break;
    case "electric":
      color = "yellow-400";
      break;
    case "psychic":
      color = "pink-400";
      break;
    case "ice":
      color = "blue-300";
      break;
    case "dragon":
      color = "purple-500";
      break;
    case "dark":
      color = "yellow-800";
      break;
    case "steel":
      color = "gray-600";
      break;
    case "fairy":
      color = "pink-500";
      break;
  }

  return color;
};

export default function pokemon({ pokeman }) {
  return (
    <Layout title={pokeman.name}>
      <div className="bg-blue-900 rounded-lg p-10 text-white">
        <h1 className="text-4xl mb-2 text-center capitalize font-bold">
          {pokeman.name}
        </h1>
        <img className="mx-auto" src={pokeman.image} alt={pokeman.name} />
        <p>
          <span className="font-bold mr-2">Weight: </span>
          {Math.floor(pokeman.weight / 4.5359237)} (lbs)
        </p>
        <p>
          <span className="font-bold mr-2">Height: </span>
          {pokeman.height}
        </p>
        <h2 className="font-bold text-2xl mt-6 mb-2">Types: </h2>

        {pokeman.types.map((type, index) => (
          <span
            key={index}
            className={`bg-${TypeToColor(type.type.name)} hover:bg-${
              TypeToColor(type.type.name).slice(0, -3) + "500"
            } text-white font-bold py-2 px-4 rounded-full mr-1`}
          >
            {type.type.name}
          </span>
        ))}
        <p className="mt-10 text-center">
          <Link href="/">
            <a>
              <button className="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded">
                Back Home
              </button>
            </a>
          </Link>
        </p>
      </div>
    </Layout>
  );
}

export async function getServerSideProps({ query }) {
  const id = query.id;
  try {
    const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
    const pokeman = await res.json();
    const paddedIndex = ("00" + id).slice(-3);
    const image = `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${paddedIndex}.png`;
    pokeman.image = image;

    return {
      props: { pokeman },
    };
  } catch (err) {
    console.error(err);
  }
}
