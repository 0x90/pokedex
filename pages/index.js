import Link from "next/link";
import Layout from "../components/Layout";
import React from "react";

export default function Home({ pokemon }) {
  return (
    <Layout title="NextJS - PokeDex">
      <h1 className="text-4xl mb-8 text-center">NextJS - PokeDex</h1>

      <ul class="flex flex-col bg-gray-300 p-4">
        {pokemon.map((pokeman, index) => (
          <Link href={`/pokemon?id=${index + 1}`}>
            <a>
              <li class="border-gray-400 flex flex-row mb-2">
                <div class="select-none cursor-pointer bg-gray-200 rounded-md flex flex-1 items-center p-4  transition duration-500 ease-in-out transform hover:-translate-y-1 hover:shadow-lg">
                  <div class="flex flex-col rounded-md bg-gray-300 justify-center items-center mr-4">
                    <img
                      className="w-20 h-20 mr-3"
                      src={pokeman.image}
                      alt={pokeman.name}
                    />
                  </div>
                  <div class="flex-1 pl-1 mr-16">
                    <div class="font-medium">{index + 1}.</div>
                    <div class="text-gray-600 text-sm capitalize">
                      {pokeman.name}
                    </div>
                  </div>
                  <div class="text-gray-600 text-xs">
                    Click to see <strong>{pokeman.name}</strong> details
                  </div>
                </div>
              </li>
            </a>
          </Link>
        ))}
      </ul>
    </Layout>
  );
}

export async function getStaticProps(context) {
  try {
    const res = await fetch("https://pokeapi.co/api/v2/pokemon?limit=151");
    const { results } = await res.json();
    const pokemon = results.map((result, index) => {
      const paddedIndex = ("00" + (index + 1)).slice(-3);
      const image = `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${paddedIndex}.png`;
      return {
        ...result,
        image,
      };
    });
    return {
      props: { pokemon },
    };
  } catch (err) {
    console.error(err);
  }
}
